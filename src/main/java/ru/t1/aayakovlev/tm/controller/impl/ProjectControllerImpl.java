package ru.t1.aayakovlev.tm.controller.impl;

import ru.t1.aayakovlev.tm.controller.BaseController;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.service.ProjectService;
import ru.t1.aayakovlev.tm.service.ProjectTaskService;

import java.util.Arrays;
import java.util.List;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;
import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextNumber;

public final class ProjectControllerImpl implements BaseController {

    private final ProjectService service;

    private final ProjectTaskService projectTaskService;

    public ProjectControllerImpl(ProjectService service, ProjectTaskService projectTaskService) {
        this.service = service;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void changeStatusById() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        System.out.println("Statuses to enter:");
        System.out.println(Arrays.toString(Status.values()));
        System.out.print("Enter new status: ");
        final String statusValue = nextLine();
        final Status status = Status.toStatus(statusValue);
        final Project project = service.changeStatusById(id, status);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void changeStatusByIndex() {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        System.out.println("Statuses to enter:");
        System.out.println(Arrays.toString(Status.values()));
        System.out.print("Enter new status: ");
        final String statusValue = nextLine();
        final Status status = Status.toStatus(statusValue);
        final Project project = service.changeStatusByIndex(index, status);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void clear() {
        System.out.println("[CLEAR PROJECTS]");
        service.deleteAll();
        System.out.println("[OK]");
    }

    @Override
    public void completeById() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        final Project project = service.changeStatusById(id, Status.COMPLETED);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void completeByIndex() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        final Project project = service.changeStatusByIndex(index, Status.COMPLETED);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void create() {
        System.out.println("[CREATE PROJECT]");
        System.out.print("Enter name: ");
        final String name = nextLine();
        System.out.print("Enter description: ");
        final String description = nextLine();
        final Project project = service.create(name, description);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeById() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        final Project project = service.findById(id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        projectTaskService.removeProjectById(id);
        System.out.println("[OK]");
    }

    @Override
    public void removeByIndex() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        final Project project = service.findByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        projectTaskService.removeProjectById(project.getId());
        System.out.println("[OK]");
    }

    private void show(final Project project) {
        if (project == null) return;
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + Status.toName(project.getStatus()));
    }

    @Override
    public void showAll() {
        System.out.println("[SHOW ALL PROJECTS]");
        final List<Project> projects = service.findAll();
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
    }

    @Override
    public void showById() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        final Project project = service.findById(id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        show(project);
        System.out.println("[OK]");
    }

    @Override
    public void showByIndex() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        final Project project = service.findByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        show(project);
        System.out.println("[OK]");
    }

    @Override
    public void startById() {
        System.out.println("[START PROJECT BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        final Project project = service.changeStatusById(id, Status.IN_PROGRESS);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void startByIndex() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        final Project project = service.changeStatusByIndex(index, Status.IN_PROGRESS);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void updateById() {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        System.out.print("Enter new name: ");
        final String name = nextLine();
        System.out.print("Enter new description: ");
        final String description = nextLine();
        final Project project = service.updateById(id, name, description);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void updateByIndex() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        System.out.print("Enter new name: ");
        final String name = nextLine();
        System.out.print("Enter new description: ");
        final String description = nextLine();
        final Project project = service.updateByIndex(index, name, description);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
