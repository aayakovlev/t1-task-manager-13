package ru.t1.aayakovlev.tm.controller.impl;

import ru.t1.aayakovlev.tm.controller.CommandController;
import ru.t1.aayakovlev.tm.model.Command;
import ru.t1.aayakovlev.tm.service.CommandService;

import static ru.t1.aayakovlev.tm.util.FormatUtil.format;

public final class CommandControllerImpl implements CommandController {

    private final CommandService commandService;

    public CommandControllerImpl(CommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("email: aayakovlev@t1-consulting.ru");
        System.out.println("name: Yakovlev Anton.");
    }

    @Override
    public void showArgumentError() {
        System.err.println("[ERROR]");
        System.err.println("Passed argument not recognized...");
        System.err.println("Try 'java -jar task-manager.jar -h' for more information.");
        System.exit(1);
    }

    @Override
    public void showCommandError() {
        System.err.println("[ERROR]");
        System.err.println("Passed command not recognized...");
        System.err.println("Try 'help' for more information.");
    }

    @Override
    public void showExit() {
        System.out.println("[EXIT]");
        System.exit(0);
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getCommands();
        for (final Command command : commands)
            System.out.println(command);
    }

    @Override
    public void showInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final long availableCores = runtime.availableProcessors();
        final long maxMemory = runtime.maxMemory();
        final long freeMemory = runtime.freeMemory();
        final long totalMemory = runtime.totalMemory();
        final long usedMemory = totalMemory - freeMemory;

        final boolean checkMaxMemory = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = checkMaxMemory ? "no limit" : format(maxMemory);
        final String freeMemoryFormat = format(freeMemory);
        final String totalMemoryFormat = format(totalMemory);
        final String usedMemoryFormat = format(usedMemory);

        System.out.println("[INFO]");
        System.out.println("Available cores: " + availableCores);
        System.out.println("Max memory: " + maxMemoryFormat);
        System.out.println("Total memory: " + totalMemoryFormat);
        System.out.println("Free memory: " + freeMemoryFormat);
        System.out.println("Used memory: " + usedMemoryFormat);
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.13.0");
    }

    @Override
    public void showWelcome() {
        System.out.println(
                "___________              __                                                        \n" +
                "\\__    ___/____    _____|  | __   _____ _____    ____ _____     ____   ___________ \n" +
                "  |    |  \\__  \\  /  ___/  |/ /  /     \\\\__  \\  /    \\\\__  \\   / ___\\_/ __ \\_  __ \\\n" +
                "  |    |   / __ \\_\\___ \\|    <  |  Y Y  \\/ __ \\|   |  \\/ __ \\_/ /_/  >  ___/|  | \\/\n" +
                "  |____|  (____  /____  >__|_ \\ |__|_|  (____  /___|  (____  /\\___  / \\___  >__|   \n" +
                "               \\/     \\/     \\/       \\/     \\/     \\/     \\//_____/      \\/       ");
    }
}

