package ru.t1.aayakovlev.tm.controller.impl;

import ru.t1.aayakovlev.tm.controller.BaseController;
import ru.t1.aayakovlev.tm.controller.TaskController;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.service.TaskService;

import java.util.Arrays;
import java.util.List;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;
import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextNumber;

public final class TaskControllerImpl implements TaskController {

    private final TaskService service;

    public TaskControllerImpl(TaskService service) {
        this.service = service;
    }

    @Override
    public void changeStatusById() {
        System.out.println("[CHANGE TASK STATUS BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        System.out.println("Statuses to enter:");
        System.out.println(Arrays.toString(Status.values()));
        System.out.print("Enter new status: ");
        final String statusValue = nextLine();
        final Status status = Status.toStatus(statusValue);
        final Task task = service.changeStatusById(id, status);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void changeStatusByIndex() {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        System.out.println("Statuses to enter:");
        System.out.println(Arrays.toString(Status.values()));
        System.out.print("Enter new status: ");
        final String statusValue = nextLine();
        final Status status = Status.toStatus(statusValue);
        final Task task = service.changeStatusByIndex(index, status);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void clear() {
        System.out.println("[CLEAR TASKS]");
        service.deleteAll();
        System.out.println("[OK]");
    }

    @Override
    public void completeById() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        final Task task = service.changeStatusById(id, Status.COMPLETED);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void completeByIndex() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        final Task task = service.changeStatusByIndex(index, Status.COMPLETED);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void create() {
        System.out.println("[CREATE TASK]");
        System.out.print("Enter name: ");
        final String name = nextLine();
        System.out.print("Enter description: ");
        final String description = nextLine();
        final Task task = service.create(name, description);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeById() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        final Task task = service.removeById(id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeByIndex() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        final Task task = service.removeByIndex(index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    private void renderTasks(final List<Task> tasks) {
        int index = 1;
        for (final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index++ + ". " + task);
        }
    }

    private void show(final Task task) {
        if (task == null) return;
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Project id: " + task.getProjectId());
        System.out.println("Status: " + Status.toName(task.getStatus()));
    }

    @Override
    public void showAll() {
        System.out.println("[SHOW ALL TASKS]");
        final List<Task> tasks = service.findAll();
        renderTasks(tasks);
    }

    @Override
    public void showById() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        final Task task = service.findById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        show(task);
        System.out.println("[OK]");
    }

    @Override
    public void showByIndex() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        final Task task = service.findByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        show(task);
        System.out.println("[OK]");
    }

    @Override
    public void showByProjectId() {
        System.out.println("[SHOW TASKS BY PROJECT ID]");
        System.out.print("Enter projectId: ");
        final String projectId = nextLine();
        final List<Task> tasks = service.findByProjectId(projectId);
        renderTasks(tasks);
        System.out.println("[OK]");
    }

    @Override
    public void startById() {
        System.out.println("[START TASK BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        final Task task = service.changeStatusById(id, Status.IN_PROGRESS);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void startByIndex() {
        System.out.println("[START TASK BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        final Task task = service.changeStatusByIndex(index, Status.IN_PROGRESS);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void updateById() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        System.out.print("Enter new name: ");
        final String name = nextLine();
        System.out.print("Enter new description: ");
        final String description = nextLine();
        final Task task = service.updateById(id, name, description);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void updateByIndex() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        System.out.print("Enter new name: ");
        final String name = nextLine();
        System.out.print("Enter new description: ");
        final String description = nextLine();
        final Task task = service.updateByIndex(index, name, description);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
