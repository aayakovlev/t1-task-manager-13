package ru.t1.aayakovlev.tm.service.impl;

import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;
import ru.t1.aayakovlev.tm.repository.TaskRepository;
import ru.t1.aayakovlev.tm.service.ProjectTaskService;

import java.util.List;

public class ProjectTaskServiceImpl implements ProjectTaskService {

    final ProjectRepository projectRepository;

    final TaskRepository taskRepository;

    public ProjectTaskServiceImpl(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (taskId == null || taskId.isEmpty()) return;
        if (!projectRepository.existsById(projectId)) return;
        final Task task = taskRepository.findById(taskId);
        if (task == null) return;
        task.setProjectId(projectId);
    }

    @Override
    public void removeProjectById(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (!projectRepository.existsById(projectId)) return;
        final List<Task> tasks = taskRepository.findAllByProjectId(projectId);
        for (final Task task : tasks) taskRepository.removeById(task.getId());
        projectRepository.removeById(projectId);
    }

    @Override
    public void unbindTaskFromProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (taskId == null || taskId.isEmpty()) return;
        if (!projectRepository.existsById(projectId)) return;
        final Task task = taskRepository.findById(taskId);
        if (task == null) return;
        task.setProjectId(null);
    }

}
