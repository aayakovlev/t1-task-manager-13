package ru.t1.aayakovlev.tm.service;

import ru.t1.aayakovlev.tm.enumerated.Status;

import java.util.List;

public interface BaseService<T> {

    T changeStatusById(final String id, final Status status);

    T changeStatusByIndex(final Integer index, final Status status);

    void deleteAll();

    List<T> findAll();

    T findById(final String id);

    T findByIndex(final Integer index);

    T removeById(final String id);

    T removeByIndex(final Integer index);

    void save(final T e);

}
