package ru.t1.aayakovlev.tm.service;

public interface ProjectTaskService {

    void bindTaskToProject(final String projectId, final String taskId);

    void removeProjectById(final String projectId);

    void unbindTaskFromProject(final String projectId, final String taskId);

}
