package ru.t1.aayakovlev.tm.model;

import ru.t1.aayakovlev.tm.enumerated.Status;

import java.util.UUID;

public final class Task {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description = "";

    private String projectId;

    private Status status = Status.NOT_STARTED;

    public Task() {
    }

    public Task(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return name + " : " + description;
    }

}
