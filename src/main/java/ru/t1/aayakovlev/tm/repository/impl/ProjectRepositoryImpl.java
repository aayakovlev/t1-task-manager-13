package ru.t1.aayakovlev.tm.repository.impl;

import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRepositoryImpl implements ProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public int count() {
        return projects.size();
    }

    @Override
    public Project create(final String name) {
        final Project project = new Project();
        project.setName(name);
        return save(project);
    }

    @Override
    public Project create(final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return save(project);
    }

    @Override
    public void deleteAll() {
        projects.clear();
    }

    @Override
    public boolean existsById(String id) {
        for (final Project project : projects) {
            if (project.getId().equals(id)) return true;
        }
        return false;
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public Project findById(final String id) {
        for (final Project project : projects) {
            if (project.getId().equals(id)) return project;
        }
        return null;
    }

    @Override
    public Project findByIndex(final Integer index) {
        return projects.get(index);
    }

    @Override
    public Project remove(final Project project) {
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeById(final String id) {
        final Project project = findById(id);
        if (project == null) return null;
        return remove(project);
    }

    @Override
    public Project removeByIndex(final Integer index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        return remove(project);
    }

    @Override
    public Project save(final Project project) {
        projects.add(project);
        return project;
    }

}
