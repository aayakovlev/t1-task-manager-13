package ru.t1.aayakovlev.tm.repository.impl;

import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepositoryImpl implements TaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public int count() {
        return tasks.size();
    }

    @Override
    public Task create(final String name) {
        final Task task = new Task();
        task.setName(name);
        return save(task);
    }

    @Override
    public Task create(final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return save(task);
    }

    @Override
    public void deleteAll() {
        tasks.clear();
    }

    @Override
    public boolean existsById(String id) {
        for (final Task task : tasks) {
            if (task.getId().equals(id)) return true;
        }
        return false;
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public Task findById(final String id) {
        for (final Task task : tasks) {
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    @Override
    public Task findByIndex(final Integer index) {
        return tasks.get(index);
    }

    @Override
    public Task remove(final Task task) {
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(String projectId) {
        final List<Task> projectTasks = new ArrayList<>();
        for (final Task task : tasks) {
            if (task.getProjectId() == null) continue;
            if (task.getProjectId().equals(projectId)) projectTasks.add(task);
        }
        return projectTasks;
    }

    @Override
    public Task removeById(final String id) {
        final Task task = findById(id);
        if (task == null) return null;
        return remove(task);
    }

    @Override
    public Task removeByIndex(final Integer index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        return remove(task);
    }

    @Override
    public Task save(final Task task) {
        tasks.add(task);
        return task;
    }

}
